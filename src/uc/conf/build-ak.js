// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/ak/extension out=..\built\ak.js wrap=true
({
    baseUrl: "..",
    include: ['forms/ak/e_ak'],
    name: "optimizers/almond",
    out: "..\\built\\ak.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/txt'
      }
    }
})