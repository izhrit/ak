﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/map/e_ak_map.html'
	, 'forms/ak/todo/c_ak_todo'
],
function (c_fastened, tpl, c_ak_todo)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self = this;

			$(sel + ' button.print').button({ icon: 'ui-icon-print' }).click(function () { self.OnPrint(); });
		}

		controller.OnPrint = function ()
		{
			c_ak_todo().ShowModal();
		}

		controller.colModel =
		[
			  { label: 'Пр', name: 'Проект', width:45 }
			, { label: '№ Дела', name: 'Номер_дела' }
			, { label: 'Регион', name: 'Регион' }
			, { label: 'Должник', name: 'Должник' }
			, { label: 'СК', name: 'СК' }
			, { label: 'Торги', name: 'Торги' }
			, { label: 'Суд', name: 'Суд' }
			, { label: 'Состояние', name: 'Состояние' }
		];

		controller.PrepareGridRows= function(rows)
		{
			var grid_rows = [
				{
					Проект: 'РД'
					, Номер_дела: 'А15-4278/2015'
					, Регион: 'Республика Дагестан'
					, Должник: 'Имранов А М'
					, Суд: '12.09.2020'
					, Состояние: 'отправить отчёт кредиторам'
				}
				,{
					Проект: 'КП'
					, Номер_дела: 'А14-345/2015'
					, Регион: 'Беловодье'
					, Должник: 'ООО Рога и копыта'
					, СК: '11.07.2020'
					, Торги: '15.07.2020'
					, Состояние: 'подготовить положение о торгах'
				}
			];
			return grid_rows;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'local'
				, data: this.PrepareGridRows(this.fastening.model)
				, colModel: this.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Процедуры {0} - {1} из {2}'
				, emptyText: 'Нет процедур для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#cpw-ctrl-ak-map-grid-pager'
				, viewrecords: true
				, height: 'auto'
				, width: '800'
				, multiselect: true
				, multiboxonly: true
				, multiselectWidth: 35
				, ignoreCase: true
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		return controller;
	}
});