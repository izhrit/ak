﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/procedure/requests/e_ak_requests.html'
	, 'forms/ak/todo/c_ak_todo'
],
function (c_fastened, tpl, c_ak_todo)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;

			$(sel + ' button.print').button({icon: 'ui-icon-print'});
			$(sel + ' button.envelopes').button({ icon: 'ui-icon-mail-closed' }).click(function () { self.OnEnvelopes(); });

			$(sel + ' button.state').button();

			$(sel + ' button.edit').button({ icon: 'ui-icon-pencil' });
			$(sel + ' button.download').button({ icon: 'ui-icon-disk' });

			$(sel + ' div.state a').click(function () { self.ShowTodo(); });
			$(sel + ' div.addressee a').click(function () { self.ShowTodo(); });
		}

		controller.OnEnvelopes = function ()
		{
			this.ShowTodo();
		}

		controller.ShowTodo = function ()
		{
			c_ak_todo().ShowModal();
		}

		return controller;
	}
});