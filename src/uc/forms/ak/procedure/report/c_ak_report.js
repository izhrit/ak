﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/procedure/report/e_ak_report.html'
	, 'forms/ak/todo/c_ak_todo'
],
function (c_fastened, tpl, c_ak_todo)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;

			$(sel + ' button.print').button({icon: 'ui-icon-print'});
			$(sel + ' button.edit').button({ icon: 'ui-icon-pencil' });
			$(sel + ' button.download').button({ icon: 'ui-icon-disk' });

			$(sel + ' div.navigation button').button({ icon: 'ui-icon-pencil' }).click(function () { self.ShowTodo(); });
		}

		controller.ShowTodo = function ()
		{
			c_ak_todo().ShowModal();
		}

		return controller;
	}
});