﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/procedure/props/e_ak_proc_props.html'
	, 'forms/ak/todo/c_ak_todo'
],
function (c_fastened, tpl, c_ak_todo)
{
	return function ()
	{
		var tpl_options =
		{
			field_spec:
			{
				  Общие: { controller: c_ak_todo }
				, Дополнительно: { controller: c_ak_todo }
				, Меры: { controller: c_ak_todo }
			}
		};
		var controller = c_fastened(tpl, tpl_options);
		controller.size = {width:900,height:600};
		return controller;
	}
});