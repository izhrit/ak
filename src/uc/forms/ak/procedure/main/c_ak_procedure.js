﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/procedure/main/e_ak_procedure.html'
	, 'forms/ak/procedure/report/c_ak_report'
	, 'forms/ak/procedure/requests/c_ak_requests'
	, 'forms/ak/todo/c_ak_todo'
	, 'forms/ak/procedure/props/c_ak_proc_props'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, c_ak_report, c_ak_requests, c_ak_todo, c_ak_proc_props, h_msgbox)
{
	return function ()
	{
		var tpl_options =
		{
			field_spec:
			{
				  Запросы: { controller: c_ak_requests }
				, Отчёт: { controller: c_ak_report }
				, Анализ: { controller: c_ak_todo }
				, Кредиторы: { controller: c_ak_todo }
				, Финансы: { controller: c_ak_todo }
				, Имущество: { controller: c_ak_todo }
				, Делопроизводство: { controller: c_ak_todo }
				, Витрина: { controller: c_ak_todo }
			}
		};
		var controller = c_fastened(tpl, tpl_options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;

			$(sel + ' button.properties').button({ icon: 'ui-icon-wrench' }).click(function () { self.OnProperties(); });

			$(sel + ' div.cpw-ctrl-ak-main-procedure > div.header select').select2();
		}

		controller.OnProperties = function ()
		{
			h_msgbox.ShowModal({title:'Реквизиты процедуры',controller:c_ak_proc_props()});
		}

		return controller;
	}
});