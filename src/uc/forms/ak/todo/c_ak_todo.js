﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/todo/e_ak_todo.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		controller.size = { width: 550, height: 250 };

		controller.ShowModal = function ()
		{
			h_msgbox.ShowModal({title:'Функционал недоступен',controller:this});
		}

		return controller;
	}
});