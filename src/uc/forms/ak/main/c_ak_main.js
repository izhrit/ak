﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/ak/main/e_ak_main.html'
	, 'forms/ak/procedure/main/c_ak_procedure'
	, 'forms/ak/todo/c_ak_todo'
	, 'forms/ak/map/c_ak_map'
],
function (c_fastened, tpl, c_ak_procedure, c_ak_todo, c_ak_map)
{
	return function ()
	{
		var tpl_options =
		{
			field_spec:
			{
				  Процедура: { controller: c_ak_procedure }
				, Все_процедуры: { controller: c_ak_map }
				, Фронт_работ: { controller: c_ak_todo }
			}
		};
		var controller = c_fastened(tpl, tpl_options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;

			$(sel + ' button.register').button({ icon: 'ui-icon-circle-plus' }).click(function () { self.OnRegister(); });
			$(sel + ' button.account').button({icon:'ui-icon-person'}).click(function () { self.OnAccount(); });
		}

		controller.OnRegister = function ()
		{
			c_ak_todo().ShowModal();
		}

		controller.OnAccount = function ()
		{
			c_ak_todo().ShowModal();
		}

		return controller;
	}
});