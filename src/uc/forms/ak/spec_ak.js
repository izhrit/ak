define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var items_spec = {

		controller:
		{
			"main":
			{
				path: 'forms/ak/main/c_ak_main'
				, title: 'Главная страница'
			}
			,"proc":
			{
				path: 'forms/ak/procedure/main/c_ak_procedure'
				, title: 'Работа с выбранной процедурой'
			}
		}
	};
	return h_spec.combine(items_spec);
});