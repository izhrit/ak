define([
	  'forms/base/codec/codec'
],
function (BaseCodec)
{
	return function ()
	{
		var res = BaseCodec();

		res.SafeCopyField = function (from, to, field_name_from, field_name_to, default_value)
		{
			var field_name_from_parts = field_name_from.split('.');
			if (field_name_from_parts.length > 1)
			{
				var last_i = field_name_from_parts.length - 1;
				for (var i = 0; from && null != from && i < last_i; i++)
				{
					var field_name_from_part = field_name_from_parts[i];
					from = !from[field_name_from_part] ? null : from[field_name_from_part];
				}
				field_name_from = field_name_from_parts[last_i];
			}
			if (null!=from && from[field_name_from])
			{
				if (!field_name_to)
					field_name_to = field_name_from;
				to[field_name_to] = res.Copy(from[field_name_from]);
			}
			else if (default_value || '' == default_value)
			{
				if (!field_name_to)
					field_name_to = field_name_from;
				to[field_name_to] = default_value;
			}
		}

		res.CopyArray = function (data)
		{
			var res = [];
			for (var i = 0; i < data.length; i++)
			{
				var field_value = data[i];
				var res_field_value = this.Copy(field_value);
				res.push(res_field_value);
			}
			return res;
		}

		res.CopyFieldsTo = function (data,res)
		{
			for (var field_name in data)
			{
				var field_value = data[field_name];
				var res_field_value = this.Copy(field_value);
				res[field_name] = res_field_value;
			}
		}

		res.CopyObject = function (data)
		{
			var res = {}
			this.CopyFieldsTo(data, res);
			return res;
		}

		res.Copy = function (data)
		{
			if (!data && data != '')
			{
				return null;
			}
			else if ('[object Array]' === Object.prototype.toString.call(data))
			{
				return res.CopyArray(data);
			}
			else if ('object' == typeof data)
			{
				return res.CopyObject(data);
			}
			else
			{
				return data;
			}
		}

		res.Encode = function (data)
		{
			return this.Copy(data);
		};

		res.Decode = function (json_string)
		{
			return this.Copy(data);
		};

		return res;
	}
});
