﻿define([
	  'forms/base/fastened/test/fft_abstract'
]
, function (fft_abstract)
{
	var default_fastened_field_tester = fft_abstract();

	default_fastened_field_tester.match = function (dom_item, tag_name, fc_type)
	{
		return true;
	}

	default_fastened_field_tester.check_value = function (dom_item, value)
	{
		var evalue= dom_item.val();
		if (value==evalue)
		{
			return null; // 'ok, value is "' + value + '"';
		}
		else
		{
			return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
		}
	}

	default_fastened_field_tester.simple_set_value = function (dom_item, value)
	{
		dom_item.val(value);
		dom_item.change();
		dom_item.trigger('focusout');
	}

	default_fastened_field_tester.set_value = function (dom_item, value)
	{
		var next_input_label = dom_item.next('.input-label');
		if (1 != next_input_label.length)
		{
			this.simple_set_value(dom_item, value);
		}
		else
		{
			var current_value = dom_item.val();
			if ('' == current_value && '' == value || '' != current_value && '' != value)
			{
				this.simple_set_value(dom_item, value);
			}
			else
			{
				var transitionstart_events = "transitionstart webkitTransitionStart oTransitionStart MSTransitionStart";
				var on_transitionstart= null;
				on_transitionstart= function (e)
				{
					if (wbt.count_to_transition)
					{
						wbt.count_to_transition++;
					}
					else
					{
						wbt.count_to_transition = 1;
						wbt.count_transitioned= 0;
					}
					var transitionend_events= "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd";
					var on_transitionend= null;
					on_transitionend= function (e)
					{
						wbt.count_transitioned++;
						next_input_label.off(transitionend_events, on_transitionend);
					};
					next_input_label.on(transitionend_events, on_transitionend);
					next_input_label.off(transitionstart_events, on_transitionstart);
				};
				next_input_label.on(transitionstart_events, on_transitionstart);
				dom_item.val(value);
				dom_item.change();
				if ('date' == dom_item.attr('fc-type'))
					dom_item.datepicker('disable');
				dom_item.focus();
				dom_item.focusout();
				next_input_label.focus();
				if ('date' == dom_item.attr('fc-type'))
					dom_item.datepicker('enable');
			}
		}
		return 'set value "' + value + '"';
	}

	return default_fastened_field_tester;
});