{
	"Куратор": {
		"Фамилия": "Неклюдова"
		,"Имя": "Вероника"
		,"Отчество": "Алексеевна"
	}
	,"Учитель": {
		"Математики": {
			"Фамилия": "Соловьёв"
			,"Имя": "Иван"
			,"Отчество": "Петрович"
		}
	}
	,"Звёздочки":[
		{
			"Название": "Камызяки"
			,"Ученики":[
				{
					"Фамилия":"Косолапов"
					,"Имя":"Михаил"
					,"Отчество":"Сергеевич"
					,"За_поведение": "2"
					,"Меры_воздействия": "взять на буксир"
				}
				,{
					"Фамилия":"Савиных"
					,"Имя":"Арина"
					,"Отчество":"Петровна"
					,"За_поведение": "2"
					,"Меры_воздействия": "взять на буксир"
				}
				,{
					"Фамилия":"Санникова"
					,"Имя":"Мария"
					,"Отчество":"Алексеевна"
					,"За_поведение": "2"
					,"Меры_воздействия": "взять на буксир"
				}
				,{
					"Фамилия":"Сидоров"
					,"Имя":"Леонид"
					,"Отчество":"Михайлович"
					,"За_поведение": "2"
					,"Меры_воздействия": "взять на буксир"
				}
				,{
					"Фамилия":"Макаров"
					,"Имя":"Сергей"
					,"Отчество":"Алексеевич"
					,"За_поведение": "2"
					,"Меры_воздействия": "взять на буксир"
				}
			]
		}
		,{
			"Название": "Орлёнок"
			,"Ученики":[
				{
					"Фамилия":"Сергеев"
					,"Имя":"Пётр"
					,"Отчество":"Алексеевич"
					,"За_поведение": "2"
					,"Меры_воздействия": "взять на буксир"
				}
			]
		}
	]
	,"Мероприятия":[
		"Поход"
		,"Субботник"
	]
}