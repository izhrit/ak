﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/fastened/fields/e_fastened_fields.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});