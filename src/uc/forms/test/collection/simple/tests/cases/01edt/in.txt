include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions

wait_text "пуст"
shot_check_png ..\..\shots\00new.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\00new.json.result.txt

goto "?edit=test.collection.collection-1"
include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Иванов"
shot_check_png ..\..\shots\01edt-1.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01edt-1.json.result.txt

goto "?edit=test.collection.collection-2"
include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Петров"
shot_check_png ..\..\shots\01edt-2.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01edt-2.json.result.txt

goto "?edit=test.collection.collection-3"
include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "Сидоров"
shot_check_png ..\..\shots\01edt-3.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01edt-3.json.result.txt

exit