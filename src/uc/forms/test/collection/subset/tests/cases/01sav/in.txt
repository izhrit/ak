include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "из списка"
shot_check_png ..\..\shots\00new.png

je $('div.cpw-collection select').val(0).change();
je $('div.cpw-collection select').val(1).change();

shot_check_png ..\..\shots\01sav.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\simple\tests\contents\01edt-2.json.result.txt
exit